unit ScriptTemplate.GuidGenerator;

interface

uses System.Classes, System.SysUtils, ToolsAPI, CodeTemplateAPI, ScriptTemplate.Base;

type TScriptGuidGenerator = class(TScriptTemplateBase, IOTACodeTemplateScriptEngine)

  private
    function generateGuid: string;

  public
    procedure Execute(const ATemplate   : IOTACodeTemplate;
                      const APoint      : IOTACodeTemplatePoint;
                      const ASyncPoints : IOTASyncEditPoints;
                      const AScript     : IOTACodeTemplateScript;
                      var Cancel        : Boolean); override;

    function GetIDString: WideString; override;
    function GetLanguage: WideString; override;
end;

implementation

{ TScriptGuidGenerator }

type IInterfaceName = interface(IInterface) ['{F5E2AC56-F400-4AB5-ACEA-780A6C837DE8}']

end;

procedure TScriptGuidGenerator.Execute(const ATemplate   : IOTACodeTemplate;
                                       const APoint      : IOTACodeTemplatePoint;
                                       const ASyncPoints : IOTASyncEditPoints;
                                       const AScript     : IOTACodeTemplateScript;
                                       var Cancel        : Boolean);
var
  p: IOTACodeTemplatePoint;
begin
  inherited;
  p := FTemplate.FindPoint('guid');

  if Assigned(P) then
  begin
    P.Editable := False;
    P.Value    := Format( '''%s''', [generateGuid]);
  end;
end;

function TScriptGuidGenerator.generateGuid: string;
var
  guid : TGUID;
  HRes : HResult;
begin
  HRes := CreateGUID(guid);
  if HRes = S_OK then
    result := GUIDToString(guid).ToUpper;
end;

function TScriptGuidGenerator.GetIDString: WideString;
begin
  result := '{8434F4B7-7833-49D4-8724-CD2C36F1170C}';
end;

function TScriptGuidGenerator.GetLanguage: WideString;
begin
  result := 'guidGenerator';
end;

end.
