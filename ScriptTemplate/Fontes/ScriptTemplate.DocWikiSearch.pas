unit ScriptTemplate.DocWikiSearch;

interface

uses System.Classes, System.SysUtils, ToolsAPI, CodeTemplateAPI, ScriptTemplate.Base, Winapi.ShellAPI,
     Winapi.Windows;

const
  URL        = 'http://docwiki.embarcadero.com/Libraries/Rio/e/index.php?search=%s';
  POINT_NAME = 'searchFor';

type TScriptDocWikiSearch = class(TScriptTemplateBase, IOTACodeTemplateScriptEngine)

  public
    procedure Execute(const ATemplate   : IOTACodeTemplate;
                      const APoint      : IOTACodeTemplatePoint;
                      const ASyncPoints : IOTASyncEditPoints;
                      const AScript     : IOTACodeTemplateScript;
                      var Cancel        : Boolean); override;

    function GetIDString: WideString; override;
    function GetLanguage: WideString; override;
end;

implementation

{ TScriptDocWikiSearch }

procedure TScriptDocWikiSearch.Execute(const ATemplate   : IOTACodeTemplate;
                                       const APoint      : IOTACodeTemplatePoint;
                                       const ASyncPoints : IOTASyncEditPoints;
                                       const AScript     : IOTACodeTemplateScript;
                                       var Cancel        : Boolean);
var
  p         : IOTASyncEditPoint;
  searchFor : string;
begin
  inherited;
  p := FindSyncPoint(POINT_NAME);

  if Assigned(p) then
  begin
    searchFor := p.Text;
    if not searchFor.IsEmpty then
      ShellExecute(0, 'OPEN', PChar( Format(URL, [searchFor])), nil, nil, SW_SHOWNORMAL );
  end;
end;

function TScriptDocWikiSearch.GetIDString: WideString;
begin
  result := '{36F042B6-95C3-4763-AAF3-E596F697EBFD}';
end;

function TScriptDocWikiSearch.GetLanguage: WideString;
begin
  result := 'DocWikiSearch';
end;

end.
