unit ScriptTemplate.Register;

interface

uses ToolsAPI,
     CodeTemplateAPI,
     ScriptTemplate.GuidGenerator,
     ScriptTemplate.DocWikiSearch;

procedure Register;

implementation

procedure Register;
var
  registerTemplate: IOTACodeTemplateServices;
begin
  registerTemplate := (BorlandIDEServices as IOTACodeTemplateServices);

  registerTemplate.RegisterScriptEngine(TScriptGuidGenerator.New);
  registerTemplate.RegisterScriptEngine(TScriptDocWikiSearch.New);
end;

end.
