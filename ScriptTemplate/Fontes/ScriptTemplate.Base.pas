unit ScriptTemplate.Base;

interface

uses System.Classes, System.SysUtils, ToolsAPI, CodeTemplateAPI;

type TScriptTemplateBase = class abstract(TNotifierObject, IOTACodeTemplateScriptEngine)

  protected
    FTemplate   : IOTACodeTemplate;
    FPoint      : IOTACodeTemplatePoint;
    FSyncPoints : IOTASyncEditPoints;
    FScript     : IOTACodeTemplateScript;

    function getScriptTemplate: TStringList;
    function getScriptValue(const AName: string): string;

    function FindSyncPoint(const APointName: String): IOTASyncEditPoint;

  public
    procedure Execute(const ATemplate   : IOTACodeTemplate;
                      const APoint      : IOTACodeTemplatePoint;
                      const ASyncPoints : IOTASyncEditPoints;
                      const AScript     : IOTACodeTemplateScript;
                      var   Cancel      : Boolean); virtual;

    function GetIDString: WideString; virtual; abstract;
    function GetLanguage: WideString; virtual; abstract;

    class function New: IOTACodeTemplateScriptEngine;
end;

implementation

{ TScriptTemplateBase }

procedure TScriptTemplateBase.Execute(const ATemplate   : IOTACodeTemplate;
                                      const APoint      : IOTACodeTemplatePoint;
                                      const ASyncPoints : IOTASyncEditPoints;
                                      const AScript     : IOTACodeTemplateScript;
                                      var   Cancel      : Boolean);
begin
  FTemplate   := ATemplate;
  FPoint      := APoint;
  FSyncPoints := ASyncPoints;
  FScript     := AScript;
  Cancel      := False;

  if not Assigned(ATemplate) then
    exit;
end;

function TScriptTemplateBase.FindSyncPoint(const APointName: String): IOTASyncEditPoint;
var
  i: Integer;
begin
  result := nil;
  if not Assigned(FTemplate) then
    Exit;

  for i := 0 to Pred(FSyncPoints.Count) do
    if FSyncPoints.Points[i].Name.ToLower.Equals(APointName.ToLower) then
      Exit(FSyncPoints.Points[i]);
end;

function TScriptTemplateBase.getScriptTemplate: TStringList;
begin
  result := TStringList.Create;
  if Assigned(FScript) then
    Result.Text := FScript.Script;
end;

function TScriptTemplateBase.getScriptValue(const AName: string): string;
var
  listScript: TStringList;
  i         : Integer;
begin
  listScript := getScriptTemplate;
  try
    for i := 0 to Pred(listScript.Count) do
    begin
      if listScript.Names[i].ToLower.Equals(AName.ToLower) then
      begin
        result := listScript.Values[AName];
        Break;
      end;
    end;
  finally
    listScript.Free;
  end;
end;

class function TScriptTemplateBase.New: IOTACodeTemplateScriptEngine;
begin
  result := Self.Create;
end;

end.
